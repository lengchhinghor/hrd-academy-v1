package com.example.springbootsecuritydemo.repository;

import com.example.springbootsecuritydemo.model.Role;
import com.example.springbootsecuritydemo.model.User;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;


@Mapper
public interface UserRepository {
    @Select("select * from users where username = #{username}")
    @Result(property = "id", column = "id")
    @Result(property = "roles", column = "id", many = @Many(select = "getRolesByUserId"))
    User loadUserByUsername(String username);

    @Select("select r.id, r.name from users_roles as ur inner join roles as r on ur.role_id = r.id where ur.user_id = #{userId}")
    List<Role> getRolesByUserId(Integer userId);
}
