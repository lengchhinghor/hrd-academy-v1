package com.example.springbootsecuritydemo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Role {
    private int id;
    private ERole name;
}
