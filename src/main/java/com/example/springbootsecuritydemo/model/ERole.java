package com.example.springbootsecuritydemo.model;

public enum ERole {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_SUPER_ADMIN
}
